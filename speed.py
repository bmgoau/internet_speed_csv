### Internet speed test script for Python 2.6+
### 
### This runs continuously downloading the test file every 5 minutes 
### generating a csv file that you can use in a scatter chart in Excel.
###

from __future__ import print_function
from time import time, sleep
from datetime import datetime
from os.path import exists
import urllib2
import os
import sys

    
### CONFIGURATION

repeat_minutes = 2
download_file = "http://mirror.internode.on.net/pub/test/10meg.test"
file_bytes = 10000000 # This must be the exact size of the download file in bytes.

# Windows:
output_file = "internet_speed.csv"

# Linux:
#output_file = "/home/username/internet_speed.csv"

###

if not exists(output_file):
    try:
        with open(output_file, "w") as f:
            f.write("Time,Speed (MB/s)\n")
    except IOError:
        print ("Please edit the configuration. Output file location does not exist.")
        input()
        exit()

file_name = download_file.split('/')[-1]
i = 0
while True:
    try:

        start_time = time()
        
        u = urllib2.urlopen(download_file)
        f = open(file_name, 'wb')
        meta = u.info()
        file_size = int(meta.getheaders("Content-Length")[0])
        file_size_dl = 0
        block_sz = 8192
        while True:
            i += 1
            buffer = u.read(block_sz)
            if not buffer:
                break

            file_size_dl += len(buffer)
            f.write(buffer)
            status = r"%10d  [%3.2f%%]" % (file_size_dl, file_size_dl * 100. / file_size)
            status = status + chr(8)*(len(status)+1)
            if i == 100:
                print (status)
                i = 0

        f.close()

        seconds_taken = time() - start_time
        mbytes_per_second = str( round(file_bytes / seconds_taken / 1024 / 1024, 2) )
        print (mbytes_per_second," MB/s")
        with open(output_file, "a") as f:
            f.write(datetime.now().strftime("%Y-%m-%d %H:%M:00")+','+mbytes_per_second+'\n')
        print ("Waiting ",repeat_minutes," minutes.")
        sleep((repeat_minutes * 60) - (time() - start_time))
    except:
        print ("Unexpected error:", sys.exc_info()[0])
        continue

